import numpy as np
import pandas as pd
city_map_list={'A': [4, 9, 10, 11, 13, 14, 16, 18, 19, 20, 26, 29, 30, 31, 32, 35, 37, 38, 39, 40, 41, 42, 43, 44, 47, 49, 50, 51, 52, 53, 54, 56, 57, 61, 62, 64, 65, 66, 67, 68, 75, 77, 79, 80, 93, 99, 104, 107, 108, 110], 'B': [16, 20], 'C': [11, 18, 27, 28, 32, 33, 34, 36, 38, 39, 45, 49, 53, 57, 58, 63, 66, 69, 70, 76, 77, 89, 90, 103, 108, 109, 112, 118, 132], 'D': [11, 13, 20, 39], 'E': [7], 'F': [18, 21, 22, 53, 70, 85, 95, 105, 107, 108, 109, 112, 120, 121, 122, 123, 124, 128, 131, 132, 133, 134, 135, 136, 137, 143, 145, 146, 147, 148, 149, 162, 163, 164, 165,177, 178, 179, 185, 186, 192, 195, 196, 211, 212, 230, 235, 242, 246, 252, 256, 261, 267, 271, 277, 278, 283, 284, 292, 296, 303, 316, 319], 'G': [11, 25], 'H': [4,  21, 47, 48, 49], 'I': [26], 'J': [2], 'K': [5, 9, 10, 23, 26, 30, 33, 39, 40, 46]}
for city in city_map_list.keys():
    area_list=city_map_list[city]#改
    initial=pd.read_csv("predict_{}_v4.csv".format(city),names=["区域","天数","新增感染人数"])#改
    replace=pd.read_csv("predict_adjust_{}.csv".format(city),names=["区域","天数","新增感染人数"])#改
    initial=initial[~initial["区域"].isin(area_list)]
    replace=replace.loc[replace["区域"].isin(area_list),:]
    result=pd.concat([initial,replace],ignore_index=True)
    result=result.sort_values(by=["区域","天数"])
    print(result)
    result.to_csv("predict_{}_v5.csv".format(city),index=False,header=False)
