import pandas as pd
import numpy as np
from keras import layers
from keras.layers.normalization import BatchNormalization
from keras import Input
from keras.models import Model
from keras import callbacks
import keras.optimizers as opt
from keras import regularizers
import matplotlib.pyplot as plt
from check_data import quyu_list

callback_list=[
    callbacks.EarlyStopping(monitor="loss",patience=200),
    callbacks.ReduceLROnPlateau(monitor="loss",factor=0.85,verbose=1,patience=12)
]

infetion_A_all=pd.read_csv("infection_A.csv",names=["城市","区域","日期","新增感染人数"])
infetion_A_all=infetion_A_all[["日期","区域","新增感染人数"]]
string_list=["less_than_200","200_350","350_520","520_850","850_1300","1300_2600","2600_5500","greater_equal_5500"]
for i in range(3,4):
    infetion_A=infetion_A_all[infetion_A_all["区域"].isin(quyu_list[i])]
    print(infetion_A)
    lower=infetion_A.groupby("日期")["新增感染人数"].sum()
    label=np.array(lower)
    print(label)
    print(label.shape)

    train=np.arange(1,61)
    train=(train-train.mean())/train.std()
    print(train)
    print(train.shape)

    data_input=Input(shape=(1,))
    x=layers.Dense(32,activation="relu")(data_input)
    x=layers.Dense(32,activation="relu")(x)
    x=layers.Dense(32,activation="relu")(x)
    #x = layers.Dense(16, activation="relu")(x)
    predict_inf=layers.Dense(1)(x)

    predict_inf_model=Model(data_input,predict_inf)
    predict_inf_model.compile(optimizer=opt.adam(),loss="mse")
    predict_inf_model.fit(train,label,epochs=40000,batch_size=60,callbacks=callback_list)
    predict_inf_model.save("toy_v5_{}.h5".format(string_list[i]))
    predict_data=np.arange(1,91)
    predict_data=(predict_data-predict_data.mean())/predict_data.std()
    pre=predict_inf_model.predict(predict_data)
    plt.figure()
    plt.plot(train, label, label="data")
    plt.plot(predict_data,pre,label="model")
    plt.legend(loc="best")
    plt.savefig("F:\\referance_function_picture\\function_{}.png".format(string_list[i]))
    plt.close("all")

