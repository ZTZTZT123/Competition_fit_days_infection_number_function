import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
city_result=[]
citys=["A","B","C","D","E","F","G","H","I","J","K"]
city_map={"A":118,"B":30,"C":135,"D":75,"E":34,"F":331,"G":38,"H":53,"I":33,"J":8,"K":48}
date_list=[ 21200630,
21200701,
21200702,
21200703,
21200704,
21200705,
21200706,
21200707,
21200708,
21200709,
21200710,
21200711,
21200712,
21200713,
21200714,
21200715,
21200716,
21200717,
21200718,
21200719,
21200720,
21200721,
21200722,
21200723,
21200724,
21200725,
21200726,
21200727,
21200728,
21200729
]
date={i:j for i,j in zip(range(61,91),date_list)}
def to_int(x):
    return int(x)
def new_increase(x):
    if(x==0):
        return 0
    else:
        return int(x)+1
def to_date(x):
    return date[x]
for i in citys:
    city_result.append(pd.read_csv("predict_{}_v4.csv".format(i),names=["区域", "天数", "新增感染人数"]))


for i in range(11):
    print(city_result[i])
for city,result in zip(citys,city_result):
    result["城市"]=city
    city_result[citys.index(city)]=result.iloc[:,[3,0,1,2]]
for i in range(len(city_result)):
    city_result[i].iloc[:,1:3]=city_result[i].iloc[:,1:].applymap(to_int)
    city_result[i].iloc[:, 3:] = city_result[i].iloc[:, 3:].applymap(new_increase)
    city_result[i].iloc[:,2:3]=city_result[i].iloc[:,2:3].applymap(to_date)
for i in city_result:
    print(i)
final=pd.concat(city_result,ignore_index=True)
print(final)
final.to_csv("F:\\result_baidu\\result_v50000.csv",index=False,header=False)


